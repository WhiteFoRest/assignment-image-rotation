#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "image_io.h"

// Число, на которое должен начинаться любой bmp-файл (в little-endian "MB")
static const uint16_t BMP_START_NUMBER = 0x4D42;
static const uint16_t BMP_RESERVED = 0;  // Зарезервированное число, постоянно 0
static const uint32_t BMP_HEADER_SIZE = 54;  // Размер header (Signature + info)
static const uint32_t BMP_HEADER_INFO_SIZE = 40;  // Размер header (info)
static const uint16_t BMP_PLANES = 1;  // Всегда равен 1
static uint16_t BMP_COLOR_DEPTH = 24; // Глубина цвета
static uint16_t BMP_COMPRESSION = 0; // без компрессии

// Читает из bmp_file_name в file и выводит статус загрузки (image_load_status)
static enum image_load_status file_read_bmp(char* bmp_file_name, FILE** file){
    *file = fopen(bmp_file_name, "rb");

    if (file == NULL) {
        if (errno == ENOENT)
            return IMAGE_LOAD_FILE_NOT_EXIST;
        return IMAGE_LOAD_READ_FAIL;
    }
    return IMAGE_LOAD_OK;
}

// Возвращает padding по заданной ширине
static uint32_t get_padding (uint32_t width) {
    uint32_t width_bytes = width * 3;
    return (width_bytes % 4 == 0 ? 0 : (4 - width_bytes % 4));
}

// Читает из file только header и возвращает статус загрузки (image_load_status)
static enum image_load_status read_header(FILE* file, struct bmp_file_header* header, uint32_t count){
    size_t header_count = fread(header, sizeof(struct bmp_file_header), count, file);
    if (header_count != count) 
        return IMAGE_LOAD_READ_FAIL;
    return IMAGE_LOAD_OK;
}

// Проверка header на константные значения
static enum image_load_status validate_header(struct bmp_file_header* header){
    if (header->width == 0 || header->height == 0)
        return IMAGE_LOAD_READ_FAIL;

    if (header->type != BMP_START_NUMBER)
        return IMAGE_LOAD_TYPE_UNSUPPORTED;

    if (header->bits_per_pixel != BMP_COLOR_DEPTH)
        return IMAGE_LOAD_BPP_NOT_SUPPORTED;

    if (header->compression_method != 0)
        return IMAGE_LOAD_COMPRESSION_NOT_SUPPORTED;

    if (header->color_planes_num != 1 || header->reserved != 0)
        return IMAGE_LOAD_READ_FAIL;
    
    return IMAGE_LOAD_OK;
}

//Читаем данные (пиксели) в назначенную картинку
static enum image_load_status read_data(FILE* file, struct image* dst){
    const uint32_t width = (dst)->width;
    const uint32_t height = (dst)->height;
    const uint32_t padding = get_padding(width);

    if (width == 0 || height == 0)
        return IMAGE_LOAD_FILE_NOT_EXIST;

    dst->data_array = realloc(dst->data_array, sizeof(struct pixel) * width * height);

    if (dst->data_array == NULL) 
        return IMAGE_LOAD_READ_FAIL;

    struct pixel* pointer = dst->data_array;

    for (size_t i = 0; i < height; i++) {
        size_t pixels_count = fread(pointer, sizeof(struct pixel), width, file);
        if (pixels_count != width) {
            free(pointer);
            return IMAGE_LOAD_READ_FAIL;
      }
      size_t padding_skip = fseek(file, padding, SEEK_CUR); // в случае успеха fseek() возвращает 0

      if (padding_skip != 0) {
        free(pointer);
        return IMAGE_FSEEK_ERROR;
      }

      pointer = pointer + width;
    }

    return IMAGE_LOAD_OK;
}

/*
    Читает bmp-файл в указанную ссылку на ссылку на изображение.
    Непосредственный указатель (*dst, имеющий тип image*)
    будет заменен указателем на созданное изображение.
    Изображение после использования следует освободить с помощью image_destroy(...)

    Возвращает код, описывающий результат чтения.
    Непосредственный указатель на изображение устанавливается,
    только если чтение прошло успешно.
*/
enum image_load_status image_read_from_bmp(char* bmp_file_name, struct image* dst) {
    FILE* file;
    file_read_bmp(bmp_file_name, &file);

    struct bmp_file_header header;

    enum image_load_status header_status = read_header(file, &header, 1);
        if (header_status != IMAGE_LOAD_OK) return header_status;

    enum image_load_status validate_status = validate_header(&header);
        if (validate_status != IMAGE_LOAD_OK) return validate_status;

    (dst)->width = header.width;
    (dst)->height = header.height;
    
    enum image_load_status data_status = read_data(file, dst);
    
    fclose(file);
    return data_status;
}

// Возвращает длину с padding
static uint32_t get_width_padded(uint32_t width_bytes){
    return width_bytes + get_padding(width_bytes);
}

// Возвращает размер файла
static uint32_t get_file_size(uint32_t width_padded, struct image* src){
    return BMP_HEADER_SIZE + width_padded * image_get_height(src);
}

// Читает из bmp_file_name в file и выводит статус сохранения (image_save_status)
static enum image_save_status file_write_bmp(char* bmp_file_name, FILE** file){
    *file = fopen(bmp_file_name, "wb");

    if (file == NULL) {
        if (errno == EACCES)
            return IMAGE_SAVE_NO_ACCESS;
        return IMAGE_SAVE_OPEN_FAIL;
    }
    return IMAGE_SAVE_OK;
}

// Выводит размер данных картинки (в байтах)
static uint32_t get_data_size(uint32_t width, uint32_t height) {
    return width*3 * height + get_padding(width) * height;
}

// Записывает в file новый header, сформированный с помощью длины, ширины, padding, image*
static enum image_save_status write_header(FILE* file, uint32_t width, uint32_t height, struct image* src){
    struct bmp_file_header header = {
        header.type = BMP_START_NUMBER, // В начале BMP-файла всегда должно быть это число
        header.file_size = get_file_size(get_width_padded(width), src), // Размер файла
        header.reserved = BMP_RESERVED,    // Зарезервированное поле, должно быть 0
        /* 
            Смещение файла, с которого начинаются пиксельные данные.
            Так как мы пишем пиксели сразу после заголовка, то указываем смещение,
            равное размеру этого заголовка. 
        */
        header.data_start_addr = BMP_HEADER_SIZE,
        header.header_size = BMP_HEADER_INFO_SIZE, 
        header.width = width,    // Ширина картинки в пикселях
        header.height = height,  // Высота картинки в пикселях
        header.color_planes_num = BMP_PLANES,    // В соответствии с описанием BMP, всегда равен 1
        header.bits_per_pixel = BMP_COLOR_DEPTH,     // Сколько нужно бит, чтобы закодировать 1 пиксель (8 бит * 3 цвета)
        header.compression_method = BMP_COMPRESSION,  // 0 = нет компрессии
        header.image_size = get_data_size(width, height),          // This is the size of the raw bitmap data; a dummy 0 can be given for BI_RGB bitmaps.
        header.x_pixels_per_meter = 0,  // the horizontal resolution of the image
        header.y_pixels_per_meter = 0,  // the vertical resolution of the image
        header.colors_num = 0, // Если это значение равно нулю, то в растре используется максимально возможное количество цветов, которые разрешены значением BMP_COLOR_DEPTH
        header.important_colors_num = 0,// Количество важных цветов, 0 = все цвета важны
    };
    size_t header_count = fwrite(&header, sizeof(struct bmp_file_header), 1, file);

    if (header_count != 1)
        return IMAGE_SAVE_HEADER_FAIL;
    return IMAGE_SAVE_OK;
}

// Записывает в file данные (пиксели) по width, height, padding, struct pixel* pointer
static enum image_save_status write_data(FILE* file, uint32_t width, uint32_t height, uint32_t padding, struct pixel* pointer) {

    for (size_t i = 0; i < height; i++) {

        const size_t pixels_count = fwrite(pointer, sizeof(struct pixel), width, file);

        if (pixels_count != width)
            return IMAGE_SAVE_DATA_FAIL;

        const size_t padding_count = fwrite(pointer, padding, 1, file);

        if ((padding_count != 1 && padding != 0) || (padding_count != 0 && padding == 0))
            return IMAGE_PADDING_FAIL;

        pointer = pointer + width;
    }
    return IMAGE_SAVE_OK;

}

/*
    Записывает изображение в файл в формате bmp
    Возвращает код, описывающий результат записи
*/
enum image_save_status image_save_to_bmp(char* bmp_file_name, struct image* src) {
    FILE* file;
    file_write_bmp(bmp_file_name, &file);

    const uint32_t height = image_get_height(src);
    const uint32_t width = image_get_width(src);
    const uint32_t padding = get_padding(width);

    enum image_save_status write_status = write_header(file, width, height, src);
    
    if (write_status == IMAGE_SAVE_OK) {
        write_status = write_data(file, width, height, padding, src->data_array);
    }
    fclose(file);
    return write_status;
}

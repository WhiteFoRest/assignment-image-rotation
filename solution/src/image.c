#include <stdlib.h>
#include <string.h>

#include "image.h"

// Возвращает индекс пикселя в массиве data_array
static size_t get_pixel_index(uint32_t width, uint32_t x, uint32_t y){
    return (width * y) + x;
}

// Возвращает, сколько в изображении пикселей
static size_t get_pixels_size (uint32_t width, uint32_t height){
    return (width * height);
}

static void image_fill_black(struct image* new_image){
    const size_t pixels_size = get_pixels_size(new_image->width, new_image->height);
    // NOLINTNEXTLINE(clang-analyzer-security.insecureAPI.DeprecatedOrUnsafeBufferHandling)
	memset(new_image->data_array, 0, pixels_size*sizeof(struct pixel));
}

/*
    Создаёт изображение, выделяя под него память.
    Изображение должно создаваться только через эту функцию
    (либо через функцию, использующую эту функцию).
    Изображение должно быть инициализированово и готово к работе.
    По умолчанию изображение заполнено черными прозрачными
    пикселями (r = 0, g = 0, b = 0).
    Изображение после использования следует разрушить,
    используя функцию image_destroy.
*/
struct image* image_create(uint32_t width, uint32_t height) {
    struct image* new_image = malloc(sizeof(struct image));

    new_image->width = width;
    new_image->height = height;

    new_image->data_array = malloc(get_pixels_size(width, height) * sizeof(struct pixel));

    image_fill_black(new_image);

    return new_image;
}

/*
    Создаёт копию изображения в памяти
    Возвращает ссылку на новое изображение
*/
struct image* image_copy(struct image* image) {
    if (image == NULL)
        return NULL;

    uint32_t width = image_get_width(image);
    uint32_t height = image_get_height(image);

    struct image* new_image = image_create(width, height);

    struct pixel* new_image_data = new_image->data_array;

    struct pixel* source_image_data = image->data_array;

    size_t pixels_size = get_pixels_size(width, height);

    for (size_t i = 0; i < pixels_size; i++){
        new_image_data[i] = source_image_data[i];
    }

    return new_image;
}

/*
    Освобождает память, используемую изображением.
    Ссылка, переданная в эту функцию, станет недействительной.
*/
void image_destroy(struct image* image) {
    if (image == NULL)
        return;

    if (image->data_array != NULL){
        image_destroy_data(image->data_array);
    }

    free(image);
}

/*
    Освобождает память, используемую data_array.
    Ссылка, переданная в эту функцию, станет недействительной.
*/
void image_destroy_data(struct pixel* data) {
    free(data);
    data = NULL;
}

/*
    Возвращает ширину изображения в пикселях.
*/
uint32_t image_get_width(struct image* image) {
    if (image == NULL)
        return 0;

    return image->width;
}

/*
    Возвращает высоту изображения в пикселях.
*/
uint32_t image_get_height(struct image* image) {
    if (image == NULL)
        return 0;

    return image->height;
}

/* 
    Используется только в image_get_pixel
    Для того, чтобы пользователю не передавать в аргументе пиксель по умолчанию
*/
static struct pixel image_get_pixel_or_default(struct image* image, struct point point, struct pixel default_pixel) {
    if (image == NULL)
        return default_pixel;
    
    if (!image_check_bounds(image, point))
        return default_pixel;
    
    size_t pixel_index = get_pixel_index(image_get_width(image), point.x, point.y);

    return image->data_array[pixel_index];
}

/*
    Возвращает пиксель изображения, находящийся в указанных координатах.
    Если точка находится за пределами изображения, возвращается
    прозрачный черный пиксель (r = 0, g = 0, b = 0).
*/
bool image_get_pixel(struct image* image, struct point point, struct pixel* pixel) {
    *pixel = image_get_pixel_or_default(image, point, (struct pixel) {0, 0, 0});
    if(pixel != NULL)
        return true;
    return false;
}

/*
    Устанавливает указанный писель указанному изображению в указанное место.
    Если пиксель установлен, возвращает true.
    Если точка находится за пределами изображения, возвращает false.
*/
bool image_set_pixel(struct image* image, struct point point, struct pixel* pixel) {
    if (image == NULL)
        return false;
    
    if (!image_check_bounds(image, point))
        return false;
    
    const size_t pixel_index = get_pixel_index(image_get_width(image), point.x, point.y);

    image->data_array[pixel_index] = *pixel;
    
    return true;
}

/*
    Проверяем, находится ли точка на изображении, или за её пределами.
    Если точка находится в пределах изображения, возвращает true.
    Иначе, возвращает false.
*/
bool image_check_bounds(struct image* image, struct point point) {
    if (image == NULL) {
        return false;
    }

    return (point.x >= 0 && point.x < image_get_width(image) &&
    point.y >= 0 && point.y < image_get_height(image));
}

/*
    Меняет местами ширину и высоту изображения.
    !important: Целостность пиксельных данных после вызова этой функции не гарантируется.
*/
void image_swap_size(struct image* image) {
    if (image == NULL)
        return;

    const uint32_t buffer = image->width;
    image->width = image->height;
    image->height = buffer;
}

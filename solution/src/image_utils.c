#define _USE_MATH_DEFINES

#include <math.h>
#include <stdlib.h>

// Решение для 'M_PI' : undeclared identifier 
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#include "image_utils.h"

// Переводит градусы в радианы
static double from_degrees_to_radians(double degrees){
    return (degrees) * M_PI / 180;
}

static double degrees_in_semicircle(double degrees){
    return fmod(fabs(degrees) + 45, 180);
}

// Нужно ли поменять длину и ширину при заданном угле поворота?
static bool is_swap_needed(double degrees){
    return degrees_in_semicircle(degrees) > 90.0f;
}

/*
    Поворачивает картинку на <degrees> градусов по часовой стрелке
    Для работы с изображением используются функции из image_utils.h.
*/
struct image image_rotate(struct image* image, double degrees) {
    const double radians = from_degrees_to_radians(degrees);

    bool size_swap_needed = is_swap_needed(degrees);

    uint32_t width = image_get_width(image);
    uint32_t height = image_get_height(image);

    struct image* old_image = image_copy(image);

    double center_x = (double) width / 2.0;
    double center_y = (double) height / 2.0;
    double cos_r = cos(-radians);
    double sin_r = sin(-radians);

    double transform_matrix[] = {
        cos_r, -sin_r, center_x - (center_x * cos_r) + (center_y * sin_r),
        sin_r,  cos_r, center_y - (center_x * sin_r) - (center_y * cos_r),
        0, 0, 1
    };

    if (size_swap_needed) {
        image_swap_size(image);

        width = image_get_width(image);
        height = image_get_height(image);

        transform_matrix[2] = center_x - (center_y * cos_r) + (center_x * sin_r);
        transform_matrix[5] = center_y - (center_y * sin_r) - (center_x * cos_r);
    }

    struct pixel* src_pixel = malloc(sizeof(struct pixel));

    for (uint32_t current_y = 0; current_y < height; current_y++) {

        for (uint32_t current_x = 0; current_x < width; current_x++) {

            struct point dst_point = {
                current_x,
                current_y
            };

            double src_x = dst_point.x + 0.5;
            double src_y = dst_point.y + 0.5;

            struct point src_point = {
                floor(transform_matrix[0] * src_x + transform_matrix[1] * src_y + transform_matrix[2]),
                floor(transform_matrix[3] * src_x + transform_matrix[4] * src_y + transform_matrix[5])
            };

            bool get_src_pixel;
 
            if (image_check_bounds(old_image, src_point)){
                get_src_pixel = image_get_pixel(old_image, src_point, src_pixel);
            }
            else {
                get_src_pixel = image_get_pixel(old_image, dst_point, src_pixel);
            }

            if(!get_src_pixel)
                continue; 

            image_set_pixel(image, dst_point, src_pixel);

        }
    }
    free(src_pixel);
    image_destroy(old_image);

    return *image;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "image.h"
#include "image_io.h"
#include "image_utils.h"

// TODO rotate for any degrees, now it's working only for degrees % 90 == 0
static const double DEGREES_DEFAULT = 90;

static void print_help();
static void print_load_err(enum image_load_status status);
static void print_save_err(enum image_save_status status);

int main(int argc, char** argv) {
    if (argc < 3 || argc > 4) {
        puts("Пожалуйста, укажите путь к исполняемой программе и 2 файла");
        print_help();
        return 0;
    }

    char* src_file = argv[1];
    char* dst_file = argv[2];

    double degrees = DEGREES_DEFAULT;

    if (argc == 4) {
        char* degrees_str = argv[3];
        degrees = strtod(degrees_str, NULL);
    }

    struct image* img = image_create(0, 0);

    enum image_load_status load_status = image_read_from_bmp(src_file, img);

    if (load_status == IMAGE_LOAD_OK) {
        puts("Изображение из файла было успешно прочитано");
    
        printf("Поворачиваем на %.2f градусов...\n", degrees);
        struct image new_image = image_rotate(img, degrees);

        enum image_save_status save_status = image_save_to_bmp(dst_file, &new_image);

        if (save_status == IMAGE_SAVE_OK) { 
            puts("Изображение было успешно загружено в файл назначения");
            puts("Работа была успешно завершена");
        } else {
            print_save_err(save_status);
            puts("Работа была завершена неудачно");
        }
    } else {
        print_load_err(load_status);
        puts("Работа была завершена неудачно");
    }
    image_destroy(img);
    return 0;
}

// Печатает ошибку загрузки
static void print_load_err(enum image_load_status status) {
    if (status == IMAGE_LOAD_BPP_NOT_SUPPORTED)
        puts("Такое количество бит на пиксель не поддерживается");
    else if (status == IMAGE_LOAD_COMPRESSION_NOT_SUPPORTED)
        puts("Сжатые BMP не поддерживаются");
    else if (status == IMAGE_LOAD_FILE_NOT_EXIST)
        puts("Указанный файл не существует");
    else if (status == IMAGE_LOAD_TYPE_UNSUPPORTED)
        puts("Файл в неверном формате");
    else if (status == IMAGE_LOAD_READ_FAIL)
        puts("Ошибка чтения файла");
    else if (status == IMAGE_FSEEK_ERROR)
        puts("Ошибка перемещения каретки");
    else
        puts("Неизвестная ошибка");
}

// Печатает ошибку сохранения
static void print_save_err(enum image_save_status status) {
    if (status == IMAGE_SAVE_NO_ACCESS)
        puts("Нет доступа для записи в файл");
    else if (status == IMAGE_SAVE_OPEN_FAIL)
        puts("Не удалось открыть файл для записи");
    else if (status == IMAGE_SAVE_WRITE_FAIL)
        puts("Не удалось записать в файл");
    else if (status == IMAGE_SAVE_HEADER_FAIL)
        puts("Не удалось записать header в файл");
    else if (status == IMAGE_SAVE_DATA_FAIL)
        puts("Не удалось записать данные в файл");
    else if (status == IMAGE_PADDING_FAIL)
        puts("Не удалось записать padding в файл");    
    else
        puts("Неизвестная ошибка");
}

/*
    Выводит подсказку по использованию программы для пользователя.
    
    @param cmd_name Название команды этой программы.
*/
static void print_help() {
    printf("Использование: <исполняемая программа> <src> <dst> <degrees>\nsrc - файл, который надо повернуть\ndst - файл, в который надо записать результат\ndegrees - поворот (по умолчанию %f градусов)\n", DEGREES_DEFAULT);
}

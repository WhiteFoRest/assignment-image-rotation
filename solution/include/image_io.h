#ifndef IMAGE_IO
#define IMAGE_IO

#include "image.h"

/*
    Этот файл содержит определения, необходимые
    для загрузки и сохранения изображений в различных форматах
    (в нашем случае только в bmp)
*/

/* 
    Состояния, которые могут возникнуть при загрузке изображения.
*/
enum image_load_status {
    IMAGE_LOAD_OK,
    IMAGE_LOAD_FILE_NOT_EXIST,
    IMAGE_LOAD_READ_FAIL,
    IMAGE_LOAD_TYPE_UNSUPPORTED,
    IMAGE_LOAD_BPP_NOT_SUPPORTED,   // BPP - bit per pixel
    IMAGE_LOAD_COMPRESSION_NOT_SUPPORTED,
    IMAGE_FSEEK_ERROR
};

/*
    Состояния, которые могут возникнуть при записи изображения.
*/
enum image_save_status {
    IMAGE_SAVE_OK,
    IMAGE_SAVE_NO_ACCESS,
    IMAGE_SAVE_OPEN_FAIL,
    IMAGE_SAVE_WRITE_FAIL,
    IMAGE_SAVE_HEADER_FAIL,
    IMAGE_SAVE_DATA_FAIL,
    IMAGE_PADDING_FAIL,
};

// Сохраняет bmp_file_name по указателю image* dst
enum image_load_status image_read_from_bmp(char* bmp_file_name, struct image* dst);

// Сохраняет в bmp_file_name картинку по указателю image* src
enum image_save_status image_save_to_bmp(char* bmp_file_name, struct image* src);

/* 
    Запакованная структура, повторяющая заголовок bmp-файлов
 */
struct bmp_file_header {
    
    // Signature (14 bytes)
    uint16_t type;
    uint32_t file_size; 
    uint32_t reserved;
    uint32_t data_start_addr;

    // Info header (40 bytes)
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t color_planes_num;
    uint16_t bits_per_pixel;
    uint32_t compression_method;
    uint32_t image_size;
    uint32_t x_pixels_per_meter;
    uint32_t y_pixels_per_meter;
    uint32_t colors_num;
    uint32_t important_colors_num;
} __attribute__((__packed__));

/*
    Запакованная структура, повторяющая один пиксель в bmp-файле
    В bmp-файлах цвета расположены в следующем порядке: синий, зелёный, красный
 */
struct bmp_pixel_bgr {
    uint8_t b, g, r;
} __attribute__((__packed__));

#endif

#ifndef IMAGE_UTILS
#define IMAGE_UTILS

/*
    Содержит определения, с помощью которых можно что-нибудь сделать с изображением.
*/

#include <stdbool.h>

#include "image.h"

struct image image_rotate(struct image* image, double degrees);

#endif

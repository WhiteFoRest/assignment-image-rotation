#ifndef _IMAGE_H
#define _IMAGE_H

/*
    Этот файл содержит определения, необходимые для
    непосредственных (!) операций над изображением,
    таких как:
    - Создание/удаление изображения
    - Получение ширины/высоты изображения
    - Получение и установка отдельных пикселей
*/

#include <stdbool.h>
#include <stdint.h>


/*
    Внутреннее представление пикселя.
*/
struct pixel {
    uint8_t r, g, b;
};

/*
    Внутреннее представление изображения.
    Поля структуры должны быть скрыты, доступ к
    полям осуществляется только через специальные функции, определённые ниже.
*/
struct image {
    uint32_t width;
    uint32_t height;

    // Одномерный массив, содержащий все пиксели изображения
    struct pixel* data_array;
};

/*
    Точка для обозначения координат на изображении.
*/
struct point {
    uint32_t x, y;
};

struct image* image_create(uint32_t width, uint32_t height);

struct image* image_copy(struct image* image);

void image_destroy(struct image* image);

void image_destroy_data(struct pixel* data);

uint32_t image_get_width(struct image* image);

uint32_t image_get_height(struct image* image);

void image_swap_size(struct image* image);

bool image_get_pixel(struct image* image, struct point point, struct pixel* pixel);

bool image_set_pixel(struct image* image, struct point point, struct pixel* pixel);

bool image_check_bounds(struct image* image, struct point point);

#endif
